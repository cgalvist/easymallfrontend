var urlBase = "https://dl.dropboxusercontent.com/u/83807663/easymall/";

function consultarCCs(document){
    $.getJSON(urlBase + 'centros-comerciales.json', {format: "json"}, function(data) {

        var lista = document.getElementById('listaCentros');
        var fila;
        var item;
        var logo;
        var texto;

        for( i = 0; i <= data.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            //item.setAttribute('href', 'centro-comercial.html' + '?id=' + data[i].id);
            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/centro-comercial.html?id=" + data[i].id + "')");
            texto.innerHTML = data[i].name;
            logo.setAttribute("src", data[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaCentros').listview('refresh');
        }
    });
}

function consultarCC(document, idCC){
    $.getJSON(urlBase + 'centro-comercial.json?id=' + idCC, {format: "json"}, function(data) {

        var lista = document.getElementById('listaLocales');
        var logoCC = document.getElementById('logoCC');
        var fila;
        var item;
        var logo;
        var texto;

        logoCC.setAttribute("src", data[0].centroComercial.logo);

        for( i = 0; i <= data[0].noticias.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            //item.setAttribute('href', 'centro-comercial.html' + '?id=' + data[i].id);
            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/local.html?id=" + data[0].noticias[i].id + "')");
            item.style.whiteSpace = "inherit";
            texto.innerHTML = data[0].noticias[i].info;
            logo.setAttribute("src", data[0].noticias[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaLocales').listview('refresh');
        }
    });
}

function consultarLocales(document, idLocal){
    $.getJSON(urlBase + 'locales.json?id=' + idLocal, {format: "json"}, function(data) {

        var lista = document.getElementById('listaLocales');
        var fila;
        var item;
        var logo;
        var texto;

        for( i = 0; i <= data.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/local.html?id=" + data[i].id + "')");
            texto.innerHTML = data[i].name;
            logo.setAttribute("src", data[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaLocales').listview('refresh');
        }
    });
	
	document.getElementById('botonVolver').setAttribute('onclick', "cargarPagina(parent.document, 'pages/centro-comercial.html?id=" + idLocal + "');");
}

function consultarLocal(document, idLocal){
    $.getJSON(urlBase + 'local.json?id=' + idLocal, {format: "json"}, function(data) {

        var logo = document.getElementById('logo');
        var nombre = document.getElementById('nombre');
        var numero = document.getElementById('numero');
        var piso = document.getElementById('piso');
        var horario = document.getElementById('horario');
        var descripcion = document.getElementById('descripcion');
        var tel1 = document.getElementById('tel1');
        var tel2 = document.getElementById('tel2');

        logo.setAttribute("src", data[0].logo);
		nombre.innerHTML = data[0].name;
		numero.innerHTML = data[0].number;
		piso.innerHTML = data[0].floor;
		horario.innerHTML = data[0].officeHours;
		descripcion.innerHTML = data[0].description;
		tel1.innerHTML = data[0].phone1;
		tel2.innerHTML = data[0].phone2;

		document.getElementById('botonVolver').setAttribute('onclick', "cargarPagina(parent.document, 'pages/locales.html?id=" + data[0].idCC + "');");
        
    });
}
