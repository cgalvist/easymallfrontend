
function cargarPagina(document, url){

    var main = document.getElementById('mainPrincipal');

    var iframe = document.createElement('iframe');
    iframe.setAttribute("src", url);
    iframe.setAttribute("frameborder", url);
    iframe.setAttribute("height", "100%");
    iframe.setAttribute("width", "100%");
    iframe.style.overflow = "hidden";
    iframe.style.height = "100%";
    iframe.style.width = "100%";
    iframe.style.position = "absolute";

    main.innerHTML = "";
    main.appendChild(iframe);

    $('#home').trigger( "create" );
}

function getVarsUrl(){
    var url= location.search.replace("?", "");
    var arrUrl = url.split("&");
    var urlObj={};
    for(var i=0; i<arrUrl.length; i++){
        var x= arrUrl[i].split("=");
        urlObj[x[0]]=x[1]
    }
    return urlObj;
}