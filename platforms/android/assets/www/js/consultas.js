//var urlBase = "https://dl.dropboxusercontent.com/u/83807663/easymall/";
var urlBase = "http://192.168.0.2:1337/";

function consultarCCs(document){
    //$.getJSON(urlBase + 'centros-comerciales.json', {format: "json"}, function(data) {

    $.getJSON(urlBase + 'Mall', function(data) {
        var lista = document.getElementById('listaCentros');
        var fila;
        var item;
        var logo;
        var texto;

        for( i = 0; i <= data.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            //item.setAttribute('href', 'centro-comercial.html' + '?id=' + data[i].id);
            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/centro-comercial.html?id=" + data[i].id + "')");
            texto.innerHTML = data[i].name;
            //logo.setAttribute("src", data[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaCentros').listview('refresh');
        }
    });
}

function consultarCC(document, idCC){
    $.getJSON(urlBase + 'Mall/'+ idCC, function(data) {

        var lista = document.getElementById('listaLocales');
        var logoCC = document.getElementById('logoCC');
        var fila;
        var item;
        var logo;
        var texto;

        //logoCC.setAttribute("src", data[0].centroComercial.logo);

        for( i = 0; i <= data.events.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            //item.setAttribute('href', 'centro-comercial.html' + '?id=' + data[i].id);
            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/local.html?id=" + data.events[i].id + "')");
            item.style.whiteSpace = "inherit";
            texto.innerHTML = data.events[i].name;
            //logo.setAttribute("src", data[0].events[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaLocales').listview('refresh');
        }
    });
}

function consultarLocales(document, idMall){
    $.getJSON(urlBase + 'mall/' + idMall, function(data) {

        var lista = document.getElementById('listaLocales');
        var fila;
        var item;
        var logo;
        var texto;

        for( i = 0; i <= data.stores.length; i++ ){

            fila = document.createElement('li');
            item = document.createElement('a');
            logo = document.createElement('img');
            texto = document.createElement('div');

            item.setAttribute('onclick', "cargarPagina(parent.document, 'pages/local.html?id=" + data.stores[i].id + "&mallid="+data.id+"')");
            texto.innerHTML = data.stores[i].name;
            //logo.setAttribute("src", data[i].logo);
            logo.className = 'ui-li-icon';

            item.appendChild(logo);
            item.appendChild(texto);
            fila.appendChild(item);

            lista.appendChild(fila);
            $('#listaLocales').listview('refresh');
        }
    });
}


function consultarLocal(document, idLocal){
    $.getJSON(urlBase + 'Store/' + idLocal, function(data) {

        var nombre = document.getElementById('localNombre');
        var numero = document.getElementById('localNumero');
        var encargado = document.getElementById('localEncargado');
        var telefono = document.getElementById('localTelefono');

        var mall = document.getElementById('localMall');
        var direccion = document.getElementById('localDireccion');
        var horario = document.getElementById('localHorario');

        nombre.innerHTML = 'Nombre: '+data.name;
        numero.innerHTML = 'Local: '+data.locationNumber;
        mall.innerHTML = 'Centro comercial: '+data.mall.name;
        direccion.innerHTML = 'Direccion: '+data.mall.address;
        horario.innerHTML = 'Horario: '+data.mall.openHour + ' - '+ data.mall.closeHour;
        encargado.innerHTML = 'Encargado: '+data.owner.name;
        telefono.innerHTML = 'Telefono: '+data.owner.phone;


    });
}
