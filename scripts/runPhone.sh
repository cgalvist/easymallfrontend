#! /bin/bash

#script para ejecutar el proyecto en linux en un dispositivo fisico por usb
#nota: asegurese de que el dispositivo esta en modo de depuracion y es reconocido por el SO

echo "agregando variable de entorno"

export PATH=${PATH}:/opt/android-sdk-linux/tools/

cd ..

echo "compilando proyecto"

cordova build

echo "ejecutando emulador"

cordova run android
